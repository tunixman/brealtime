# Transcript

This is the transcript of the requsts:

    GET /
      Params: [("q","Ping"),("d","Please return OK so that I know your service works.")]
      Accept:
      Status: 200 OK 0.001097363s
    GET /
      Params: [("q","Years"),("d","How many years of software development experience do you have?")]
      Accept:
      Status: 400 Bad Request 0.000130326s
    GET /
      Params: [("q","Position"),("d","Which position are you applying for?")]
      Accept:
      Status: 400 Bad Request 0.000122085s
    GET /
      Params: [("q","Status"),("d","Can you provide proof of eligibility to work in the US?")]
      Accept:
      Status: 400 Bad Request 0.000157635s
    GET /
      Params: [("q","Source"),("d","Please provide a URL where we can download the source code of your resume submission web service.")]
      Accept:
      Status: 400 Bad Request 0.00014178s
    GET /
      Params: [("q","Phone"),("d","Please provide a phone number we can use to reach you.")]
      Accept:
      Status: 400 Bad Request 0.000213159s
    GET /
      Params: [("q","Resume"),("d","Please provide a URL where we can download your resume and cover letter.")]
      Accept:
      Status: 400 Bad Request 0.000138148s
    GET /
      Params: [("q","Degree"),("d","Please list your relevant university degree(s).")]
      Accept:
      Status: 400 Bad Request 0.000160568s
    GET /
      Params: [("q","Puzzle"),("d","Please solve this puzzle:\n ABCD\nA->--\nB-->-\nC--=-\nD--<-\n")]
      Accept:
      Status: 400 Bad Request 0.000102319s
    GET /
      Params: [("q","Name"),("d","What is your full name?")]
      Accept:
      Status: 400 Bad Request 0.000090027s
    GET /
      Params: [("q","Email Address"),("d","What is your email address?")]
      Accept:
      Status: 400 Bad Request 0.000108325s
    GET /
      Params: [("q","Referrer"),("d","How did you hear about this position?")]
      Accept:
      Status: 400 Bad Request 0.000163152s
