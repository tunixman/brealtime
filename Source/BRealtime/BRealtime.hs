{-|
Module:             BRealtime
Description:        API for BRealtime code challenge.
Copyright:          © 2017 All rights reserved.
License:            AGPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module BRealtime where

import Lawless hiding ((:>))
import IO
import Questions
import Text
import Textual
import Printer
import Servant

-- | This is the @d@ query parameter.
--
-- It's basically a text message, so we parse it and print it for
-- diagnostics.
newtype Message = Message Text deriving (Show, Eq, Ord, IsString, Printable)
instance FromHttpApiData Message where
    parseQueryParam = Right ∘ Message

-- | The site root 'Handler'.
--
-- Since @q@ and @d@ are both 'QueryParam's, they may not be
-- present. 'QueryParam' represents that with 'Maybe', so we take
-- 'Maybe's for each. The 'answer' 'Iso' and the 'Answer' type both
-- handle the case where there's no 'Question', and will return an
-- error response.
root ∷ Maybe Question → Maybe Message → Handler Answer
root q d = do
    liftIO $ putStrLn $ fsep ": " [maybe "Nothing" print q, maybe "Nothing" print d, print $ q ^. answer]
    return $ q ^. answer

-- | The API type for the site root.
--
-- This requires a @GET@ request ('Get'), a 'PlainText' body, extracts
-- @q@ and @d@ from the query string if they exist, and returns an
-- 'Answer'.
--
-- Since this is a type and not a value, there needs to be an
-- implementation of this. Essentially that's the 'root' function, and
-- the API service is constructed by wrapping that with 'server', and
-- providing the 'Proxy' that 'server' can use to reflect the value at
-- runtime.
type RootAPI = QueryParam "q" Question :> QueryParam "d" Message :> Get '[PlainText] Answer

-- | The full BRealtimeAPI.
--
-- Currently this is really just 'RootAPI', but if there turns out to
-- be more, this is where we'd build the API tree.
type BRealtimeAPI = RootAPI

-- | The function that handles the 'BRealtimeAPI' service.
--
-- This is what gets called by 'server' after using the 'bRealtimeAPI'
-- 'Proxy' to reflect the correct 'Server' at runtime.
bRealtimeServer :: Server BRealtimeAPI
bRealtimeServer = root

-- | The 'Proxy' value for 'BRealtimeAPI'.
--
-- This is a dependent type witness to the 'BRealtimeAPI'. If we had
-- multiple endpoints and routing, each route would have a different
-- 'Proxy', and the 'RootAPI', after being reflected by its 'Proxy',
-- would then be able to route the subrequests to their proper
-- subhandlers.
bRealtimeAPI :: Proxy BRealtimeAPI
bRealtimeAPI = Proxy

-- | The WAI 'Application' itself.
--
-- This is the function that actually is run by the WAI stack. It
-- essentially is a type alias for a function that takes a request, a
-- function to call with a response, and then returns what that
-- function returns. This is called
-- <https://www.cs.utah.edu/~mflatt/past-courses/cs6520/public_html/s02/cps.pdf
-- Continuation Passing Style>, and originally was a compiler
-- optimization technique. WAI uses it for resource management,
-- despite there being more modern methods of handling them, such as
-- the <http://hackage.haskell.org/package/managed managed> package,
-- but the authors of WAI are slightly behind the curve of modern FP.
bRealtimeApp :: Application
bRealtimeApp = serve bRealtimeAPI bRealtimeServer
