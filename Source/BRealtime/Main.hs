{-|
Module:             Main
Description:        Main module for the bRealTime coding challenge.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Main where

import Lawless
import BRealtime
import Network.Wai.Handler.Warp
import Network.Wai.Middleware.RequestLogger (logStdoutDev)

-- | Start 'bRealtimeApp' on port 8080, logging to @stdout@.
main :: IO ()
main =
    run 8080 $ logStdoutDev bRealtimeApp
