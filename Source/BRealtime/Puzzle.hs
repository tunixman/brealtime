{-# Language TemplateHaskell, MultiWayIf #-}

{-|
Module:             Puzzle
Description:        Parse and solve the puzzle question.
Copyright:          © 2017 All rights reserved.
License:            AGPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Puzzle (
    -- * Constraint Satisfaction
    -- $csolve
    loadPuzzle,

    -- * The Puzzle Data Structures
    -- ** Unsolved and Solved (Phantom Types)
    -- $solvedph
    Unsolved,
    Solved,

    -- ** The 'Puzzle' Itself
    Puzzle,

    -- ** 'Ix' and 'At'
    -- $plookups

    -- ** Relations
    Relation,
    Relations,

    -- ** Variables
    Variable,
    variables

    ) where

import Lawless
import Textual
import Printer hiding (char)
import Parser
import Text.Read
import Control.Monad.CSP
import Map (Map)
import Set

-- $csolve
--
-- So this puzzle has four variables, and each has a relation to the others and
-- itself. The total representation is a 4x4 grid then, and we're given a
-- single pair in the question to derive the rest from.
--
-- #properties#
--
-- There are also a few properties that the puzzle has that make this slightly
-- easier:
--
-- 1. #reflexivity# Reflexivity:
--    - \(\{V_a, V_a\} → = \)
-- 2. Transitivity:
--    - \(\{ \{V_a, V_b\} → <, \{V_b, V_c\} → < \} → \{V_a, V_c\} → < \)
-- 3. #antisymm# Antisymmetricity:
--    - \( (\{V_a, V_b\} → >) → (\{V_b, V_a\} → <) \)
--
-- The obvious solution is to load the variable pairs with the given relation,
-- or the set of all relations, into a constraint solver, add these
-- suitably-expressed constraints, and let it run with it.

-- $solvedph
--
-- These are <https://wiki.haskell.org/Phantom_type Phantom
-- Types>. They mark a particular 'Puzzle' value at the type level as
-- to whether it's been 'Solved' or remains 'Unsolved'. All of the
-- functions that work with solving a 'Puzzle' are parameterized
-- specifically to 'Unsolved' 'Puzzle's. This is one of many ways of
-- giving the compiler an idea of what our expectations are of the
-- state of a value. These incur no runtime overhead, and are used
-- specifically at compile time for checking types.
--
-- Also, when you look at the type parameters of 'Puzzle', you'll
-- notice that \(s\) isn't actually in the definition at all, the
-- "right hand side" of the \(=\). It's only on the "left hand
-- side". That distinction, a type parameter only on the "left hand
-- side", is what makes a particular parameter a /Phantom Type/.

-- | Phantom type for 'Puzzles' that haven't been solved.
data Unsolved

-- | Phantom type for 'Puzzles' that have been solved.
data Solved

-- | First we need to represent the Puzzle.
--
-- It comes in as a grid with row and column headings, so we want to turn that
-- into a 'Map' of 'VPair's as labels, and the 'Domain' possibilities as
-- values.
newtype Puzzle s c d = Puzzle {unPuzzle ∷ Map (VPair c) (Relations d)}
    deriving (Eq, Ord, Show, Monoid)

row ∷ (Ord d) ⇒ Puzzle s Variable d → Variable → Row s d
row p y =
    let
        r v rl = at v ?~ rl $ mempty
    in
        Row y $ foldMapOf folded (\x → r x $ p ^. ix (VPair x y)) variables

-- | We can print a Puzzle whether it's 'Solved' or 'Unsolved'. The 'Relations'
-- 'Printable' instance handles a lot of the logic for us.
instance Printable (Puzzle s Variable Relation) where
    print p =
        let
            hdr = " " <> (hcat $ over traversed print variables) <> "\n"
            rows = fsep "\n" $ over traversed (print ∘ row p) variables
        in
            hdr <> rows

-- | A row from the 'Puzzle' over the 'Variable' y.
data Row s d = Row Variable (Map Variable (Relations d))
    deriving (Eq, Show, Ord)

-- | The @y@ coordinate of this row
rY ∷ Lens' (Row s d) Variable
rY = lens (\(Row y _) → y) (\(Row _ rs) y → (Row y rs))

instance Printable (Row s (Relations d)) where
    print r =
        let
            y = r ^. rY
            rs v = fromMaybe (print GNO) (print <$> r ^? ix v)
            vs = over traversed rs variables
        in
            print y <> hcat vs

-- | The 'Index' of a 'Row' is the @x@ 'Variable'.
type instance Index (Row s d) = Variable

-- | The 'IxValue' of a 'Row' is the 'Relations' of the @x@.
type instance IxValue (Row s d) = Relations d

-- | Lookup a 'Variable' in a 'Row'.
instance Ixed (Row s d) where
    ix k f w@(Row a rls) =
        case rls ^? ix k of
            Nothing → pure w
            Just v → f v <&> \v' → Row a (rls & ix k .~ v')

-- | Add, modify, or delete a 'Relations' for a 'Variable' in a 'Row'.
instance At (Row s d) where
    at k f w@(Row a rls) =
        let
            mv = w ^.at k
        in
            f mv <&> Row a ∘ \case
            Nothing → at k .~ Nothing $ rls
            Just v → at k ?~ v $ rls

-- $plookups
--
-- Now we add some "Control.Lens" magic to our 'Puzzle'. We're making
-- it an instance of 'Ix', which lets us look up and modify the values
-- associated with keys, and an instance of 'At', which lets us
-- insert, modify, and delete keys.

-- | The 'Index' of 'Puzzle' is the 'VPair'.
type instance Index (Puzzle s c d) = VPair c

-- | The 'IxValue', the value associated with a 'VPair' via 'Index',
-- is the 'Relations' set for that 'VPair'.
type instance IxValue (Puzzle s c d) = (Relations d)

-- | The 'Ixed' instance for 'Puzzle' will '⊕' the 'Relations' given
-- if there's already one for that 'VPair'.
instance (Ord c, Ord d) ⇒ Ixed (Puzzle s c d) where
    ix k f p =
        let
            m = unPuzzle p
            i = ix k
            mv = m ^? ix k
        in
            case mv of
                Just r → f r <&> \v → Puzzle $ m & i <>~ v
                Nothing → pure p

-- | The 'At' instance for 'Puzzle' will allow inserts, updates, and
-- deletes of the 'Relations' of a 'VPair'.
instance (Ord c, Ord d) ⇒ At (Puzzle s c d) where
    at k f p =
        let
            a = at k
            m = unPuzzle p
            t r = Puzzle
                  $ case r of
                        Nothing → m & a .~ Nothing
                        Just r' → m & a ?~ r'
            mv = p ^? ix k
        in
            t <$> f mv

-- | A 'VPair' is a pair of items that represent a unique, ordered pair of
-- 'Variables'. For now, we're treating @(A, B)@ as different from @(B,
-- A)@. When we add the constraints though, we'll express the equivalence
-- properties that should address the <#antisym Antisymmetricity>.
data VPair c = VPair {
    _vX ∷ c,
    _vY ∷ c
    }
    deriving (Eq, Ord, Show)

vX ∷ Lens' (VPair c) c
vX = lens _vX (\v x → VPair x $ _vY v)

vY ∷ Lens' (VPair c) c
vY = lens _vY (\v y → VPair (_vX v) y)

-- | The 'Domain' of a 'VPair' is the possible 'Relation's that haven't yet
-- been eliminated.
--
-- We're really wrapping 'Set' here, so we'll take advantage of its 'Foldable'
-- instance.
--
-- The Phantom Type parameter @m@ should be a maximal set for the starting 'Relations'.
newtype Relations d = Relations {unRelations ∷ Set d} deriving (Eq, Ord, Show, Foldable)

-- | This is the full set of 'Relation's that comprise an unconstrained 'Relations'.
relationsValues ∷ Relations Relation
relationsValues = foldMapOf folded (\r → mempty & contains r .~ True) $ enumFromTo GLT GGT

-- | When we're merging sets of 'Relations', we want to merge the sets.
instance (Ord d) ⇒ Monoid (Relations d) where
    mempty = Relations mempty
    a `mappend` b = a ⊕ b

-- | The 'Index' of 'Relations' is the underlying contents.
type instance Index (Relations d) = d

-- | The 'IxValue' doesn't matter because it's not a keyed collection.
type instance IxValue (Relations d) = ()

-- | 'Lens' for finding, inserting, and removing items from 'Relations'.
instance (Ord d) ⇒ Contains (Relations d) where
    contains k f r =
        let
            s = unRelations r
        in
            f (k ∈ s) <&> \c → Relations $ s & contains k .~ c

-- | 'Lens' for finding and modifying items from 'Relations'.
instance (Ord d) ⇒ Ixed (Relations d) where
    ix k f r =
        if r ^. contains k
        then f () <&> \() -> r & contains k .~ True
        else pure r

-- | 'Lens' for finding, modifying, and deleting items from 'Relations'.
instance (Ord d) ⇒ At (Relations d) where
    at k f m =
        f mv <&> \r ->
        if has _Nothing r then (m & contains k .~ False)
        else m & contains k .~ True
        where mv = bool (Just ()) Nothing $ m ^.contains k

-- | The 'Relations' will print a single 'Relation' if the set has one, or
-- 'GNO' if it has zero or more.
--
-- To avoid running the whole set, we take the first two, then take the first
-- of that, and use those to decide just what we should print. If @fs@ is @[]@,
-- but @f@ isn't, we know we have one item, and this is a fully-constrained
-- 'Relation'. Otherwise, it's either under or over constrained.
instance Printable (Relations Relation) where
    print rs =
        let
            fs = rs ^.. taking 2 folded
            f = fs ^.. taking 1 folded
        in
            if f ≡ [] ∨ fs ≢ []
            then print GNO
            else hcat $ over traversed print f

-- | The 'Relations' will parse a single 'Relation'. If it's specified, the
-- 'Relations' will contain only that 'Relation'. Otherwise, if it's 'GNO', it
-- will contain all 'Relation's.
instance Textual (Relations Relation) where
    textual =
        let
            rs = \case
                GNO → relationsValues
                r → mempty & contains r .~  True
        in
            count 1 textual
            ≫= \r → case firstOf traversed r of
                        Nothing → unexpected "Failed to parse Relation"
                        Just r' → return $ rs r'

-- | We also want to be able to reconstitute the original 'Puzzle' representation.
--
-- @hdr@ will print the header of variable names.
--
-- @row@ will then print the row leader, then the values for that row.

-- | The four 'Variable's which are components of the 'Coordinate's which have
-- relations to each other.
data Variable =
    A |
    B |
    C |
    D
    deriving (Eq, Show, Ord, Read, Enum)

-- | A list of the 'Variable's for constructing and deconstructing the 'Puzzle'.
variables ∷ [Variable]
variables = enumFromTo A D

instance Printable Variable where
    print = print ∘ show

instance Textual Variable where
    textual =
        upper ≫= \l → case readEither [l] of
                          Left e → unexpected e
                          Right q → return q

-- | The relations between the 'Variable's.
--
-- We get @>@, @<@, @=@, or @-@. @-@ is a hole we need to fill.
--
-- This follows the same ordering as 'Ordering', save that we have 'GN'. These,
-- combined with the 'Variable's, also have <#properties additional
-- properties>.
data Relation =
    GLT |
    GEQ |
    GGT |
    GNO
    deriving (Eq, Ord, Show, Enum, Bounded)

-- | Our 'Relation' can be represented directly in the original puzzle
-- statement and solution format.
instance Printable Relation where
    print = \case
        GLT → "<"
        GEQ → "="
        GGT → ">"
        GNO → "-"

-- | Our 'Relation' also can be parsed from the original.
instance Textual Relation where
    textual =
        (GLT <$ char '<') <|>
        (GEQ <$ char '=') <|>
        (GGT <$ char '>') <|>
        (GNO <$ char '-')


-- | The complete enumeration of 'VPair's. These are all the 'Puzzle'
-- constraint Relations variables.
vPairs ∷ [VPair Variable]
vPairs = [VPair a b | a ← variables, b ← variables]

-- | The Relations of a given 'Relation'. If the Relations is specified, that's the
-- value for that position. Otherwise, it can be anything.
--
-- #reflexivity#
-- Since Reflexivity is easy to do here, we just do it.
givenRelations ∷ VPair Variable → Relation → Puzzle Unsolved Variable Relation
givenRelations p r =
    mempty & at p ?~
    if
        | p ^. vX ≡ p ^. vY → mempty & contains GEQ .~ True -- Always set \({VPair_{n,n}} → \{≡\}\)
        | r ≡ GNO → relationsValues -- If \({VPair_{m,n}} ≡ \{∅\}\), set it to all relations.
        | otherwise → mempty & contains r .~ True -- If \({VPair_{m,n}} ≡ \{R\}\), set it to \(\{R\}\).

-- | Load the 'Puzzle' from our input data.
--
-- This essentially flattens the
-- incoming nested lists, maps them to the 'vPairs' labels, and then uses
-- 'givenRelations' to build the individual 'Puzzle' items. Since 'Map' is a
-- 'Monoid', we can use 'foldMapOf' to build the whole mess up.
loadPuzzle ∷ [[Relation]] → Puzzle Unsolved Variable Relation
loadPuzzle g =
    foldMapOf folded (\(p, d) → givenRelations p d)
    $ getZipList
    $ (,) <$> ZipList vPairs ⊛ (ZipList $ concatOf traversed g)
