{-|
Module:             Constraint
Description:        Solve a Puzzle using a CSP solver.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Puzzle.Constraint (
    -- * Constraint Satisfaction
    -- $csolve

    -- ** Phantom Types
    -- $solvedph
    Unsolved,
    Solved
                         ) where

import Control.Monad.CSP
import Map (Map)
import Set

-- $csolve
--
-- This puzzle has four variables, and each has a relation to the
-- others and to itself. The total representation is a 4x4 grid then,
-- and we're given a single pair in the question to derive the rest
-- from.
--
-- #properties#
--
-- There are also a few properties that the puzzle has that make this slightly
-- easier:
--
-- 1. #reflexivity# Reflexivity:
--    - \(\{V_a, V_a\} → = \)
-- 2. Transitivity:
--    - \(\{ \{V_a, V_b\} → <, \{V_b, V_c\} → < \} → \{V_a, V_c\} → < \)
-- 3. #antisymm# Antisymmetricity:
--    - \( (\{V_a, V_b\} → >) → (\{V_b, V_a\} → <) \)
--
-- The obvious solution is to load the variable pairs with the given relation,
-- or the set of all relations, into a constraint solver, add these
-- suitably-expressed constraints, and let it run with it.

-- $solvedph
--
-- These are <https://wiki.haskell.org/Phantom_type Phantom
-- Types>. They mark a particular 'Puzzle' value at the type level as
-- to whether it's been 'Solved' or remains 'Unsolved'. All of the
-- functions that work with solving a 'Puzzle' are parameterized
-- specifically to 'Unsolved' 'Puzzle's. This is one of many ways of
-- giving the compiler an idea of what our expectations are of the
-- state of a value. These incur no runtime overhead, and are used
-- specifically at compile time for checking types.
--
-- Also, when you look at the type parameters of 'Puzzle', you'll
-- notice that \(s\) isn't actually in the definition at all, the
-- "right hand side" of the \(=\). It's only on the "left hand
-- side". That distinction, a type parameter only on the "left hand
-- side", is what makes a particular parameter a /Phantom Type/.

-- | Marker for 'Puzzle's that haven't yet been solved.
data Unsolved

-- | Marker for 'Puzzle's that have been solved.
data Solved

-- | The 'Puzzle' is a mapping of 'Pair's of 'Variable's to their
-- 'Relations'. An 'Unsolved' 'Puzzle' has 'Relations' with more than
-- one 'Relation' in at least one position, while a 'Solved' 'Puzzle'
-- has exactly one in each position.
data Puzzle s c d where
    Unsolved ∷ Map (Pair c) (Relations d) → Puzzle Unsolved c d
    Solved ∷ Map (Pair c) (Relation d) → Puzzle Solved c d
    deriving (Eq, Ord, Show, Monoid)

-- | A 'Row' is a single 'Variable''s 'Relations' to all the
-- others. If the 'Row' is 'Solved', each 'Pair' has a single
-- 'Relation'.
data Row s c d where
    Unsolved ∷ Variable → Map (Pair c) (Relations d) → Puzzle Unsolved c d
    Solved ∷ Map (Pair c) (Relation d) → Puzzle Solved c d
    deriving (Eq, Ord, Show, Monoid)

row ∷ (Ord d) ⇒ Puzzle s Variable d → Variable → Row s d
row p y =
    let
        r v rl = at v ?~ rl $ mempty
    in
        Row y $ foldMapOf folded (\x → r x $ p ^. ix (VPair x y)) variables
