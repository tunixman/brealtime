{-# Language TemplateHaskell #-}

{-|
Module:             Constraints.Relations
Description:        Representations of 'Solved' and 'Unsolved' 'Relations'.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Constraints.Relations where

import Lawless hiding ((:>), (:<), imap)
import Data.Type.Combinator
import Data.Type.Fin
import Data.Type.Vector
import Type.Family.Nat
import Set

data IsSolved = Unsolved | Solved

data Relation ∷ IsSolved → * where
    RL ∷ Relation s
    RE ∷ Relation s
    RG ∷ Relation s
    deriving (Eq, Ord, Show, Enum)

data Relations ∷ IsSolved → * → * where
    Ru ∷ Set (Relation 'Unsolved) → Relations 'Unsolved (Set (Relation 'Unsolved))
    Rs ∷ Relation 'Solved → Relations 'Solved (Relation 'Solved)

type UnsolvedRelations = Relations 'Unsolved (Set (Relation 'Unsolved))
type SolvedRelations = Relations 'Solved (Relation 'Solved)

rU ∷ Iso' UnsolvedRelations (Set (Relation 'Unsolved))
rU = iso (\(Ru rs) → rs) Ru

rS ∷ Iso' SolvedRelations (Relation 'Solved)
rS = iso (\(Rs rs) → rs) Rs

-- | All 'Relation's when no 'Relation' is specified.
unspecifiedRelations ∷ UnsolvedRelations
unspecifiedRelations = view rU $ setOf folded $ enumFromTo RL RG

-- | Will convert an 'Ru' to an 'Rs' only if the 'Ru' has a single 'Relation'.
solvedRelation ∷ Prism' UnsolvedRelations SolvedRelations
solvedRelation = prism ()
-- | Our four variables
data Variable ∷ IsSolved → * where
    A ∷ Variable s
    B ∷ Variable s
    C ∷ Variable s
    D ∷ Variable s
    deriving (Eq, Ord, Show, Enum)

-- | A row of 'UnsolvedRelations' for any 'Variable'.
type UnsolvedRowRelations = (VecT N4 I UnsolvedRelations)

-- | A row of 'Relations' given by the puzzle for any 'Variable'. Some may not be
-- specified, so we use 'Maybe' 'Relation'.
type GivenRowRelations = (VecT N4 I (Maybe (Relation 'Unsolved)))

-- | Transform a 'GivenRowRelations' to an 'UnsolvedRowRelations'.
--
-- This will traverse the relations of the 'GivenRowRelations', and
-- for any that are missing, it will replace those with
-- 'unspecifiedRelations'.
unsolvedRowRelations ∷ Getter GivenRowRelations UnsolvedRowRelations
unsolvedRowRelations = to ug
    let
        ur = over traversed un
        un = maybe unspecifiedRelations (view rU ∘ view sing) ∘ review rU

        ug
-- | A row of 'SolvedRelations' for any 'Variable'.
type SolvedRowRelations = (VecT N4 I SolvedRelations)

-- | A 'Row' is a 'Variable' and its 'Relations' to the others.
data Row ∷ IsSolved → * → * → * where
    Ur ∷ Variable 'Unsolved → UnsolvedRowRelations → Row 'Unsolved (Variable 'Unsolved) UnsolvedRowRelations
    Sr ∷ Variable 'Solved → SolvedRowRelations → Row 'Solved (Variable 'Solved) SolvedRowRelations
    Gr ∷ Variable 'Unsolved → GivenRowRelations → Row 'Unsolved (Variable 'Unsolved) GivenRowRelations

type UnsolvedRow = Row 'Unsolved (Variable 'Unsolved) UnsolvedRowRelations
type SolvedRow =  Row 'Solved (Variable 'Solved) SolvedRowRelations
type GivenRow = Row 'Unsolved (Variable 'Unsolved) GivenRowRelations

uV ∷ Lens' (Row 'Unsolved (Variable 'Unsolved) UnsolvedRowRelations) (Variable 'Unsolved)
uV = lens (\(Ur v _) → v) (\(Ur _ rs) v → Ur v rs)

sV ∷ Lens' (Row 'Solved (Variable 'Solved) SolvedRowRelations) (Variable 'Solved)
sV = lens (\(Sr v _) → v) (\(Sr _ rs) v → Sr v rs)

gV ∷ Lens' (Row 'Unsolved (Variable 'Unsolved) GivenRowRelations) (Variable 'Unsolved)
gV = lens (\(Gr v _) → v) (\(Gr _ rs) v → Gr v rs)

uRs ∷ Lens' (Row 'Unsolved (Variable 'Unsolved) UnsolvedRowRelations) UnsolvedRowRelations
uRs = lens (\(Ur _ rs) → rs) (\(Ur v _) rs → Ur v rs)

sRs ∷ Lens' (Row 'Solved (Variable 'Solved) SolvedRowRelations) SolvedRowRelations
sRs = lens (\(Sr _ rs) → rs) (\(Sr v _) rs → Sr v rs)

gRs ∷ Lens' (Row 'Unsolved (Variable 'Unsolved) GivenRowRelations) GivenRowRelations
gRs = lens (\(Gr _ rs) → rs) (\(Gr v _) rs → Gr v rs)

-- | Apply the reflexive property
unsolvedReflexive ∷ UnsolvedRow → UnsolvedRow
unsolvedReflexive r =
    let
        v = r ^. uV
        ref i (I rs) = I $ if fin i ≡ fromEnum v then (Ru $ RE ^. sing) else rs
    in
        Ur (r ^. uV) (imap ref $ r ^. uRs)

-- | Build an 'UnsolvedRow' from a 'GivenRow', including the reflexive property.
unsolvedRow ∷ GivenRow → UnsolvedRow
unsolvedRow g =
    let
        v = g ^. gV
        rs = g ^. gRs
    in
        Ur $ v (over traversed un rs)

givenRow ∷ (Variable 'Unsolved) → GivenRowRelations → GivenRow
givenRow = Gr
